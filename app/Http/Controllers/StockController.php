<?php

namespace App\Http\Controllers;

use App\City;
use App\Repositories\General;
use App\User;
use Illuminate\Http\Request;


class StockController extends Controller
{
    public function directories(Request $request)
    {
        $version = $request->has('v') ? $request->get('v') : null;
        if($response = (new General())->getAll($version))
            return response()->json($response);
        return response()->json([
            'status' => false,
            'error_code' => 1,
            'error_message' => 'Nothing to receive.'
        ]);
    }

    public function stocks(User $user)
    {
        if(!$user)
            return response()->json([
                'status' => false,
                'error_code' => 1,
                'error_message' => 'This user dosen\'t exists.'
            ], 400);
        return response()->json($user->userType->stockTypes()->select('id', 'name')->get());
    }
}
