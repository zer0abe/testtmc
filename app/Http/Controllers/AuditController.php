<?php

namespace App\Http\Controllers;

use App\City;
use App\Services\AuditService;
use App\StockType;
use App\User;
use Illuminate\Http\Request;

class AuditController extends Controller
{
    public function store(Request $request)
    {
        $user = User::find($request->json('user_id'));
        $city = City::find($request->json('city_id'));
        $stockType = StockType::find($request->json('stock_type_id'));
        $barcodes = $request->json('barcodes');
        if (!$user || !$city || !$stockType)
            return response()->json([
                'status' => false,
                'error_message' => 'Wrong parameter value'],
                400);

        if(!$barcodes || !is_array($barcodes))
            return response()->json([
                'status' => false,
                'error_message' => 'Empty barcodes array'],
                400);
        $storredCounter = AuditService::storeBarcodes($user, $city, $stockType, $barcodes);
        return response()->json([
            'status' => true,
            'message' => "Successfully stored " . $storredCounter . " barcodes.",
            'stored' => $storredCounter
        ]);
    }
}
