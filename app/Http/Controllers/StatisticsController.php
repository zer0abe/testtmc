<?php

namespace App\Http\Controllers;

use App\City;
use App\Repositories\Audits;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    public function top(Request $request)
    {
        $date = $request->get('date');
        $audits = Audits::getTopTen($date);
        return response()->json($audits);
    }

    public function cities(Request$request)
    {
        $date = $request->get('date');
        return response()->json(Audits::cities($date));
    }

    public function lazy(Request $request)
    {
        if(!$date = $request->get('date'))
            return response()->json(['status' => false, 'error_message' => 'Missing required parameter "date"'], 400);
        return response()->json(Audits::lazyUsers($date));
    }

    public function staff(Request $request)
    {
        $date = $request->get('date');
        $city  = City::find($request->get('city'));
        if(!$date || !$city)
            return response()->json(['status' => false, 'error_message' => 'Missing required parameters'], 400);
        return response()->json(Audits::staff($date, $city));
    }
}
