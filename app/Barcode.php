<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barcode extends Model
{
    protected $fillable = ['value'];

    protected $dates = [];

    public function audits()
    {
        return $this->belongsToMany(Audit::class);
    }
}
