<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;

class Stock extends Model
{
    public function city()
    {
        return $this->hasOne(City::class);
    }
}
