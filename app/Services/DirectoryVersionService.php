<?php

namespace App\Services;


class DirectoryVersionService
{
    const MAX_VERSION_OCTET_VALUE = 99;
    const START_VERSION = '1.0.0';

    protected $versionString;

    public function __construct($version)
    {
        if(!is_object($version))
            $this->versionString = null;
        else
            $this->versionString = $version->version;
    }

    public function incrementVersion()
    {
        if(!$this->isVersioned())
            return $this->startVersioning();
        $versionArray = explode('.', $this->versionString);
        $versionArray = array_reverse($versionArray);
        $l = count($versionArray) - 1;
        foreach ($versionArray as $k => $v) {
            if ($k < $l && (int) $v < self::MAX_VERSION_OCTET_VALUE) {
                $versionArray[$k] = ++$v;
                break;
            } elseif ($k < $l && (int) $v == self::MAX_VERSION_OCTET_VALUE) {
                $versionArray[$k] = 0;
                if($versionArray[$k+1] == self::MAX_VERSION_OCTET_VALUE && $versionArray[$k+1] != $l) {
                    $versionArray[$k+1] = 0;
                    continue;
                } else {
                    $versionArray[$k+1]++;
                    break;
                }
            } elseif ($k == $l) {
                $versionArray[$k]++;
            }
        }
        $this->versionString = implode('.', array_reverse($versionArray));
        return $this->versionString;
    }

    private function startVersioning()
    {
        $this->versionString = self::START_VERSION;
        return $this->versionString;
    }

    private function isVersioned()
    {
        return (bool) $this->versionString;
    }
}