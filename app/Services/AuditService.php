<?php

namespace App\Services;


use App\City;
use App\StockType;
use App\User;
use App\Barcode;
use App\Audit;
use App\Repositories\Audits;

class AuditService
{
    public static function storeBarcodes(User $user, City $city, StockType $stockType, $barcodes)
    {
        $audit = Audits::getTodayAudits($user, $stockType, $city);
        $counter = 0;
        foreach ($barcodes as $barcode) {
            if(self::addBarcode($audit, $barcode))
                $counter++;
        }
        return $counter;
    }

    private static function addBarcode(Audit $audit, $code)
    {
        if($barcode = $audit->barcodes()->where('value', $code)->first() || !self::checkBarcode($code))
        {
            return false;
        }
        $barcode = new Barcode(['value' => $code]);
        $audit->barcodes()->save($barcode);
        return true;

    }

    private static function checkBarcode($barcode)
    {
        $pattern = '/^[0-9A-Z]{8}-[0-9A-Z]{4}-[0-9A-Z]{4}-[0-9A-Z]{4}-[0-9A-Z]{12}$/';
        return (bool) preg_match($pattern, $barcode);
    }
}