<?php

namespace App\Scripts;

use Illuminate\Support\Facades\Artisan;

class Installer
{
    public function install()
    {
        Artisan::call('db:seed');
        var_dump(Artisan::output());
    }
}