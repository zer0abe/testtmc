<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Audit extends Model
{
    protected $dateFormat = 'Y-m-d';

    protected $fillable = ['user_id', 'city_id', 'stock_type_id'];

    public function barcodes()
    {
        return $this->belongsToMany(Barcode::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date);
    }


}
