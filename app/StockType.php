<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockType extends Model
{
    public function userTypes()
    {
        return $this->belongsToMany(UserType::class);
    }
}
