<?php

namespace App\Providers;

use App\Repositories\Versions;
use App\Services\DirectoryVersionService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind(DirectoryVersionService::class, function () {
            return new DirectoryVersionService(Versions::getActual());
        });

    }
}
