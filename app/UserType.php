<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{

    protected $fillable = ['name', 'description'];

    public function stockTypes()
    {
        return $this->belongsToMany(StockType::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
