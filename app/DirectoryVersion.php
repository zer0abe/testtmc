<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DirectoryVersion extends Model
{
    protected $fillable = ['version'];
}
