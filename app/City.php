<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Stock;

class City extends Model
{
    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function audits()
    {
        return $this->hasMany(Audit::class);
    }
}
