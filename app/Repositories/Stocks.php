<?php

namespace App\Repositories;

use App\Stock;
use App\StockType;

class Stocks
{

    public function getRandomStockType()
    {
        return StockType::inRandomOrder()->first();
    }

    public function getArrayOfIds($limit = 0)
    {
        $ids = StockType::inRandomOrder()->select('id');
        if($limit)
            $ids->limit($limit);
        return $ids->get()->toArray();
    }
}