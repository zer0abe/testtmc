<?php

namespace App\Repositories;

use App\City;
use App\Stock;
use App\StockType;

class Cities
{
    public function all()
    {
        return City::all();
    }

    public function getRandomCity()
    {
        return City::inRandomOrder()->first();
    }
}