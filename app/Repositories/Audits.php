<?php

namespace App\Repositories;


use App\Audit;
use App\City;
use App\StockType;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Audits
{
    public static function getTodayAudits(User $user, StockType $stock, City $city)
    {
        $audit = $user->audits()->where([
                ['city_id', $city->id],
                ['stock_type_id', $stock->id],
                ['created_at', Carbon::today()->format('Y-m-d H:i:s')]
            ]
        )->first();
        if(!$audit)
            $audit = new Audit([
                'city_id' => $city->id,
                'stock_type_id' => $stock->id
            ]);
        $user->audits()->save($audit);
        return $audit;
    }

    public static function getTopTen($date = null)
    {
        $audits = Audit::withCount('barcodes');
        if($date)
            $audits->whereDate('created_at', $date);
        $audits = $audits->orderBy("barcodes_count", 'desk')
            ->limit(10)->get();
        $response = [];
        $i = 0;
        foreach ($audits as $audit) {
            $response[$i]['user_id'] = $audit->user->id;
            $response[$i]['user_name'] = $audit->user->name;
            $response[$i]['audit_id'] = $audit->id;
            $response[$i]['audit_date'] = $audit->created_at->format('Y-m-d');
            $response[$i]['barcodes_count'] = $audit->barcodes_count;
            $i++;
        }
        return $response;
    }

    public static function cities($date = null)
    {
        $cities = City::orderBy('name')->orderBy('id')->get();

        $result = [];
        $i = 0;
        foreach ($cities as $city) {
            $result[$i]['city_id'] = $city->id;
            $result[$i]['city'] = $city->name;
            if($date)
                $result[$i]['audits_count'] = $city->audits()->whereDate('created_at', $date)->count();
            else
                $result[$i]['audits_count'] = $city->audits()->count();
            $i++;
        }
        return $result;
    }

    public static function lazyUsers($date)
    {
        $query = DB::table('users')->select(DB::raw('users.id, users.name, count(audits.*) as audits_count'))
            ->join("audits", function ($join) use ($date) {
                    $join->on("users.id", "=", "audits.user_id")
                        ->where("audits.created_at", "=", $date);
                })
            ->groupBy("users.id")
            ->having(DB::raw("count(audits.*)"), '<', 5)
            ->get();
        return $query;
    }

    public static function staff($date, City $city)
    {
        $users = User::select(DB::raw('count(users.id) as user_count'))->withCount('audits')
            ->join('audits', function ($join) use ($date, $city) {
                $join->on('users.id', '=', 'audits.user_id')
                    ->where(
                        ['audits.created_at' => $date],
                        ['audits.city_id' => $city->id]
                        );
            })
            ->groupBy('audits_count')
            ->get();
        return $users;
    }
}