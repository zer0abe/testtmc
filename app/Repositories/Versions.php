<?php
namespace App\Repositories;


use App\DirectoryVersion;
use App\Services\DirectoryVersionService;

class Versions
{
    public static function createIncremented()
    {
        $versionService = resolve(DirectoryVersionService::class);
        $version = DirectoryVersion::create(['version' => $versionService->incrementVersion()]);
        return $version;
    }

    public static function getActual()
    {
        return DirectoryVersion::orderBy('id', 'desc')->first();
    }
}