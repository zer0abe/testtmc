<?php

namespace App\Repositories;

use App\DirectoryVersion;
use App\User;
use App\Stock;
use App\City;
use App\UserType;
use App\StockType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Prophecy\Exception\Doubler\ClassNotFoundException;

class General
{
    public function getAll($v)
    {
        $version = DirectoryVersion::orderBy('version', 'desc')->first();

        if ($version && $v >= $version->version)
            return false;

        if (!$version)
            $version = Versions::createIncremented();

        $response = new \stdClass();
        $response->version = $version->version;
        $response->data = new \stdClass();
        $response->data->cities = $this->getModelColumns(City::class, ['id', 'name']);
        $response->data->stockTypes = $this->getModelColumns(StockType::class, ['id', 'name']);
        $users = User::select('users.id', 'users.name', 'user_types.name as user_type')
            ->join('user_types', 'users.user_type_id', '=', 'user_types.id')
            ->get();
        $response->data->users = $users;
        return $response;
    }

    private function getModelColumns($class, $arrayOfColumns = [])
    {
        if (!class_exists($class))
            throw new ClassNotFoundException("Class $class doesn't exists", $class);
//        if (!is_array(class_parents($class, false) &&
//            !in_array('Illuminate\Database\Eloquent\Model', class_parents($class, false)))
//        )
//            throw new ModelNotFoundException("Model $class doesn't exists");
        if (!empty($arrayOfColumns)) {
            $query = $class::select($arrayOfColumns[0]);
            unset($arrayOfColumns[0]);
            if (!empty($arrayOfColumns))
                foreach ($arrayOfColumns as $column)
                    $query->addSelect($column);
            return $query->get();
        }
        return $class::all();
    }
}