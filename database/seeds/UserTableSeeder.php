<?php

use Illuminate\Database\Seeder;
use Faker\Generator;

class UserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 10)->create()->each(function ($u, Generator $faker){
            $u->name = $faker->name;
            $u->email = $faker->email;
            $u->pasword = $faker->email;
        });
    }
}
