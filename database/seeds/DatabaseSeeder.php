<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Base($faker));
        $userTypes = ['administrator', 'city_watcher', 'storekeeper'];
        factory(App\StockType::class, 10)->create();
        $stocks = new App\Repositories\Stocks();
        foreach ($userTypes as $type)
        {
                $userTypeId = \App\UserType::create([
                    'name' => $type,
                    'description' => $faker->text(200)
                ])->id;

                App\User::create([
                    'name' => $faker->name,
                    'email' => $faker->unique()->safeEmail,
                    'password' => bcrypt('123321'),
                    'remember_token' => str_random(10),
                    'user_type_id' => $userTypeId
                ]);

            if($type === 'administrator')
                $ids = $stocks->getArrayOfIds();
            else
                $ids = $stocks->getArrayOfIds(3);
            $inserts = [];
            foreach ($ids as $id)
                $inserts[] = ['user_type_id' => $userTypeId, 'stock_type_id' => $id['id']];
            DB::table('stock_type_user_type')->insert($inserts);
        }

        factory(App\City::class, 10)->create();
    }
}
