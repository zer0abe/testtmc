<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('stock_type_id');
            $table->integer('city_id');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('stock_type_id')->references('id')->on('stock_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audits', function (Blueprint $table) {
            $table->dropForeign('audits_city_id_foreign');
            $table->dropForeign('audits_stock_type_id_foreign');
            $table->dropForeign('audits_user_id_foreign');
        });
        Schema::dropIfExists('audits');
    }
}
