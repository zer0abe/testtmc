<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10)
    ];
});

$factory->define(App\Stock::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'address' => $faker->address,
        'stock_type_id' => function () {
            return (new App\Repositories\Stocks)->getRandomStockType()->id;
        },
        'city_id' => (new App\Repositories\Cities)->getRandomCity()->id
    ];
});

$factory->define(App\City::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->city
    ];
});

$factory->define(App\StockType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
    ];
});
