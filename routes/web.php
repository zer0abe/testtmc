<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/directories/', 'StockController@directories');
Route::get('/stocks/{user}', 'StockController@stocks');
//Route::get('/cities/', 'StockController@cities');
//Route::get('/users/', 'StockController@users');

Route::put('/new/city', 'DataController@addCity');
Route::put('/new/stock', 'DataController@addStockType');
Route::put('/new/user', 'DataController@addUser');
Route::put('/new/user_type', 'DataController@addUserType');

Route::put('/edit/city/{city}', 'DataController@editCity');
Route::put('/edit/stock/{stockType}', 'DataController@editStockType');
Route::put('/edit/user/{user}', 'DataController@editUser');
Route::put('/edit/user_type/{userType}', 'DataController@editUserType');

Route::delete('/remove/city/{city}', 'DataController@deleteCity');
Route::delete('/remove/stock/{stockType}', 'DataController@deleteStockType');
Route::delete('/remove/user/{user}', 'DataController@deleteUser');
Route::delete('/remove/user_type/{userType}', 'DataController@deleteUserType');

Route::put('/audit/store', 'AuditController@store');

Route::get('/statistics/top', 'StatisticsController@top');
Route::get('/statistics/cities', 'StatisticsController@cities');
Route::get('/statistics/lazy', 'StatisticsController@lazy');
Route::get('/statistics/staff', 'StatisticsController@staff');

